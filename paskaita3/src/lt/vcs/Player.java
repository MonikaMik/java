/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Monika
 */
public class Player {
    
    private String name;
    private int dice;
    private Gender gender;
    private int[] diceHand;

    //konstruktorius,neturi return, nes auto grazina
    public Player(String name, Gender gender) {
        this.name = name == null ? name : name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase();
        this.dice = random(1, 6);
        this.gender = gender;
        int [] diceHand = {random(1, 6), random(1, 6), random(1, 6), random(1, 6), random(1, 6)};
        this.diceHand = diceHand;
        
    }
    
    public String getName(){
       return this.name;
    }

    public int getDice() {
        return dice;
    }

    public Gender getGender() {
        return gender;
    }

    public int[] getDiceHand() {
        return diceHand;
    }
    


}

