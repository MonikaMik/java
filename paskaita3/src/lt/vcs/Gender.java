/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Monika
 */
public enum Gender {
    
    MALE("Male","Vyras", 1), //gender klases objektas, iskviesti: Gender.MALE
    FEMALE("Female", "Moteris", 0),
    OTHER("Other", "Kita", 2);



    private String enLabel;
    private String ltLabel;
    private int id;

    
    private Gender(String enLabel, String ltLabel, int id){
        this.enLabel = enLabel;
        this.ltLabel = ltLabel;
        this.id = id;
    }

    public String getEnLabel() {
        return enLabel;
    }

    public String getLtLabel() {
        return ltLabel;
    }
    
    public int getId() {
        return id;
    }

    public static Gender getById(int id) {
        for (Gender gen : Gender.values()) {
            if (id == gen.getId()) {
                return gen;
            }
        }
        return null;   
    }

   
}
