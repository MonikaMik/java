/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.IntStream;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Gender.*;
/**
 *
 * @author Monika
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        out("Sveiki! cia yra kauliuku zaidimas dviems asmenims");
        out("Iveskite pirmojo zaidejo varda:");
        String name1 = inWord();
        out("Pasirinkita pirmojo zaidejo lyti (moteris - 0, vyras - 1, kita - 2): ");
        Player player1 = new Player(name1, getById(inInt()) );
        out("Iveskite antrojo zaidejo varda:");
        String name2 = inWord();
        out("Pasirinkita pirmojo zaidejo lyti (moteris - 0, vyras - 1, kita - 2): ");
        Player player2 = new Player(name2, getById(inInt()));
        

        out("Rinkites zaidimo varianta (vieno kauliuko zaidimas - 1, penkiu kauliuku sumos zaidimas - 2):");
        int gameChoise = inInt();
        if (gameChoise == 1) {
            out( player1.getName() + " isrideno: " + player1.getDice());
            out( player2.getName() + " isrideno: " + player2.getDice());
            if (player1.getDice() > player2.getDice()) {
            out("Laimejo zaidejas: " + player1.getName());
            } 
            else if (player1.getDice() == player2.getDice()) {
            out("Lygiosios");
            }
            else {
            out("Laimejo zaidejas: " + player2.getName());
            }
        }
        else if (gameChoise == 2) {
            int sum = IntStream.of(player1.getDiceHand()).sum();
            int sum2 = IntStream.of(player2.getDiceHand()).sum();
            out( player1.getName() + " isrideno kauliukus: " + Arrays.toString(player1.getDiceHand()));
            out("Isridentu kauliuku suma: " + sum);
            out( player2.getName() + " isrideno kauliukus: " + Arrays.toString(player2.getDiceHand()));
            out("Isridentu kauliuku suma: " + sum2);
            if (sum > sum2) {
            out("Laimejo zaidejas: " + player1.getName());
            } 
            else if (sum == sum2) {
            out("Lygiosios");
            }
            else {
            out("Laimejo zaidejas: " + player2.getName());
            }   
        }   
    }
        

    
    
    private static void jega(){
        //jega zaidimas
        out("Sveiki atvyke i zaidima Jega!");
        out("Iveskite savo varda:");
        String name1 = inWord();
        out("Pasirinkita savo lyti (moteris - 0, vyras - 1, kita - 2): ");
        Player pl = new Player(name1, getById(inInt()) );
        
        out("Pasirinkite norima statymo suma (1-10eur):");
        int statymoSuma = inInt();
        out("Bandykite speti 6 laiminguosius skaicius nuo 1 iki 30:");
        int [] spejimai = new int [6];
        for (int i = 0; i < 6; i++) {
            spejimai[i] = inInt();
        }
        out("Spauskite Enter noredami suzinoti laiminguosius sio vakaro kamuoliukus");
        inLine();
        
        int[] laimingiejiSkaiciai = new int[6];
        int randomSk;

        for (int i = 0; i < 6; i++) {
        randomSk = (int) random(1, 30); 
        for (int j = 0; j < i; j++) {
            if (laimingiejiSkaiciai[j] == randomSk) 
            {
                randomSk = (int) random(1, 30);
                j = -1; 
            }

        }
        laimingiejiSkaiciai[i] = randomSk;
        }
        for (int i = 0; i < laimingiejiSkaiciai.length; i++)
        out(laimingiejiSkaiciai[i] + " ");
        

        out("Papildomas sio vakaro kamuoliukas:");
        int x = random(1, 30);
        for (int i = 0; i < laimingiejiSkaiciai.length; i++) {
            if (x == laimingiejiSkaiciai[i]) {
                x = (int) random(1, 30);
                i = -1;
            }
        }
        out(" " + x);
        
        out("Spauskite Enter noredami suzinoti savo laimejimo suma");
        inLine();
        
        int count = 0;
        for (int i = 0; i < laimingiejiSkaiciai.length - 1; i++) {
            for (int j = 0; j < spejimai.length - 1; j++) {
               if (laimingiejiSkaiciai[i] == spejimai[j]) {
                count++;
             }
            }
        }
        int atspejo = 0;
        for (int i = 0; i < spejimai.length - 1; i++) {
            if (spejimai[i] == x) {
                atspejo++;
            } 
        }
        
        
           if (atspejo == 1) {
                out("Jus atspejote " + count + " skaiciu(-s) ir papildoma kamuoliuka!");
                switch (count) {
                    case 0:
                    case 1:
                    case 2:
                        out("Jus nieko nelaimejote");
                        break;
                    case 3:
                        out("Jus laimejote" + (statymoSuma * 3.5));
                        break;
                    case 4:
                        out("Jus laimejote" + (statymoSuma * 68));
                        break;
                    case 5:
                        out("Jus laimejote" + (statymoSuma * 5000));
                        break;
                    default:
                        break;
                }
            }
            else if(atspejo == 0) {
                out("Jus atspejote " + count + " skaiciu(-s)!");
                switch (count) {
                    case 0:
                    case 1:
                    case 2:
                        out("Jus nieko nelaimejote");
                        break;
                    case 3:
                        out("Jus laimejote" + (statymoSuma * 1));
                        break;
                    case 4:
                        out("Jus laimejote" + (statymoSuma * 11));
                        break;
                    case 5:
                        out("Jus laimejote" + (statymoSuma * 200));
                        break;
                    case 6:
                        out("Jus laimejote" + (statymoSuma * 100000));
                        break;
                    default:
                        break;
                }
        }
    }
    
                
    
    
    
    
    private static void pradziosKodas(){
        out("Iveskite 3 raidziu zodi");
        String abc = null;
        String zxc = null;
        if (abc != null && abc.equals(zxc)) {
            out("tikrai abc");
        }else {
            out("ne abc");
        } 
        //kitaip parasyta if salyga:
        String result = abc != null && abc.equals(zxc) ? "tikrai abc" : "ne abc";
        out(result);
        out(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("'Data: ' yyyy-MM-dd 'Laikas: 'HH:mm:ss:SSS");
        out(sdf.format(new Date()));
        out(random(1, 10));
    }
    
    
    
}
