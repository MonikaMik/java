/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Monika
 */
public class Main {

    private static final String PVZ_FAILAS = "C:\\temp\\pvz.txt";
    private static final String TEMP_DIR = "C:\\temp";
    private static final String UTF_8 = "Utf-8";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File failas = new File(PVZ_FAILAS);
        returnNumbers(failas);
    }

    private static void returnNamesSorted(File file) {
        BufferedReader br = null;
        String line = "";
        List myList = new ArrayList();
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file), UTF_8));
            while ((line = br.readLine()) != null) {
                myList.add(line);
            }
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
        Collections.sort(myList);
        for (int i = 0; i < myList.size(); i++) {
            out(myList.get(i));
        }
    }
    private static void returnNamesByLetter(File file) {
        out("Iveskite norima raide");
        Scanner s= new Scanner(System.in);
        char x = s.next().charAt(0);
        BufferedReader br = null;
        String line = "";
        List myList = new ArrayList();
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file), UTF_8));
            while ((line = br.readLine()) != null) {
                myList.add(line);
            }
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
        Collections.sort(myList);
        for (int i = 0; i < myList.size(); i++) {
            String zodis = (String) myList.get(i);
                if (zodis.charAt(0) == x) {
                    out(myList.get(i));
            }
        }
    }
    private static void returnNumbers(File file) {
        BufferedReader br = null;
        String line = "";
        List myList = new ArrayList();
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(file), UTF_8));
            while ((line = br.readLine()) != null) {
                myList.add(line);
            }
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
        Collections.sort(myList);
        for (int i = 0; i < myList.size(); i++) {
            out(myList.get(i));
        }
    }
}
