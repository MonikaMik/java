/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;
import static lt.vcs.GameUtils.*;

/**
 *
 * @author Monika
 */
public class Main {

    public static int pot;
    public static String choise1;
    public static String choise2;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        out("Kauliuku pokeris");
        String p1name = inWord("Zaidejas 1, iveskite savo varda");
        String p2name = inWord("Zaidejas 2, iveskite savo varda");

        Player p1 = new Player(p1name);
        Player p2 = new Player(p2name);
        boolean zaisti = true;

        Zaisti:
        while (zaisti) {
            Game newGame = new Game(p1, p2);
            boolean game = true;
            while (game) {
                Player nugaletojas = newGame.start();
                if (p1.getCash() <= 0) {
                    out("Zaidimas baigtas, zaidejui " + p1name + " nepakanka lesu..");
                    out("Laimejo zaidejas " + p2name + "!");
                    break Zaisti;
                }
                if (p2.getCash() <= 0) {
                    out("Zaidimas baigtas, zaidejui: " + p2name + " nepakanka lesu");
                    out("Laimejo zaidejas " + p1name + "!");
                    break Zaisti;
                }
                Player pralaimetojas = getPralaimetojas(newGame, nugaletojas);
                out(nugaletojas.getName() + " jusu likutis: " + nugaletojas.getCash());
                out(pralaimetojas.getName() + " jusu likutis: " + pralaimetojas.getCash());

                int choice = inInt("ka norite daryti toliau? 0-zaisti; 1-baigti zaidima");
                if (choice == 1) {
                    break Zaisti;
                }
                
                

                bettingP1IsActive(newGame);
                bettingP2IsActive(newGame);
                
                if (choise1.equalsIgnoreCase("N")) {
                    break;
                }

                kauliukuRidenimas(newGame);

                //antras ridenimas
                String rollAgain = inLine(newGame.getActivePlayer().getName() + " ar norite pakelti statymo suma dar karta ir perristi kauliukus (taip - T; ne - N)?");
                if (rollAgain.equalsIgnoreCase("T")) {
                    bettingP1IsActive(newGame);
                    bettingP2IsActive(newGame);
                    if (choise1.equalsIgnoreCase("N")) {
                        break;
                    }

                    out(p1name + " ar norite perristi kauliukus? Jei taip iveskite T, jeigu ne - N");
                    if (inLine().equalsIgnoreCase("T")) {
                        out(p1name + " kokius kauliukus norite perristi? (parasykite kauliuku eiles nr per kableli)");
                        p1.getHand().reRollDice(inLine());
                        out("Jusu nauja kauliuku kombinacija: " + intArrayToString(p1.getHand().getHandArray()));
                        p1.setHand(p1.getHand());
                    }
                    out(p2name + " ar norite perristi kauliukus? Jei taip iveskite T, jeigu ne - N");
                    if (inLine().equalsIgnoreCase("T")) {
                        out(p2name + " kokius kauliukus norite perristi? (parasykite kauliuku eiles nr per kableli)");
                        p2.getHand().reRollDice(inLine());
                        out("Jusu nauja kauliuku kombinacija: " + intArrayToString(p2.getHand().getHandArray()));
                        p2.setHand(p2.getHand());
                    }

                    inLine("Noredami suzinoti kas laimejo, spauskite Enter");
                    out(newGame.start().getName());
                    int bonusas = nugaletojas.getHand().getCombination().getBonus();
                    out("Laimejote: " + (pot + bonusas));
                    nugaletojas.setCash(nugaletojas.getCash() + (pot / 2) + bonusas);
                    pralaimetojas.setCash(pralaimetojas.getCash() - (pot / 2));
                } else {
                    inLine("Noredami suzinoti kas laimejo, spauskite Enter");
                    out(newGame.start().getName());
                    int bonusas = nugaletojas.getHand().getCombination().getBonus();
                    out("Laimejote: " + (pot + bonusas));
                    nugaletojas.setCash(nugaletojas.getCash() + (pot / 2) + bonusas);
                    pralaimetojas.setCash(pralaimetojas.getCash() - (pot / 2));

                }

                reRollDice(newGame.getP1().getHand().getHandArray(), "1,2,3,4,5");
                reRollDice(newGame.getP2().getHand().getHandArray(), "1,2,2,4,5");

                newGame.setActivePlayer(getNextActivePlayer(newGame));

            }
        }
    }

    private static Player getPralaimetojas(Game game, Player winner) {
        if (winner.equals(game.getP1())) {
            return game.getP2();
        } else {
            return game.getP1();
        }
    }

    public static Player getNextActivePlayer(Game game) {
        if (game.getActivePlayer().equals(game.getP1())) {
            return game.getP2();
        } else {
            return game.getP1();
        }
    }

    private static int rollDice() {
        return random(1, 6);
    }

    public static void kauliukuRidenimas(Game game) {
        out("Pradekime kauliuku ridenima!");
        inLine();
        out(game.getP1().getName() + " ridena 5 kauliukus");
        out(intArrayToString(game.getP1().getHand().getHandArray()));
        inLine();
        out(game.getP2().getName() + " ridena 5 kauliukus");
        out(intArrayToString(game.getP2().getHand().getHandArray()));
    }

    public static void reRollDice(int[] hand, String dices) {
        dices = dices.replaceAll(" ", "");
        for (String dice : dices.split(",")) {
            Integer nr = new Integer(dice);
            hand[nr - 1] = rollDice();
        }
    }

    public static void bettingP1IsActive(Game game) {
        boolean play = true;
        while (play) {
            if (game.getActivePlayer().equals(game.getP1())) {
                int bet = inInt(game.getP1().getName() + ", statykite pinigu suma: ");
                while (bet <= 0 || bet > game.getP1().getCash() || bet > game.getP2().getCash()) {
                    out("Jusu statymas turi buti tarp 0 ir jusu turimos sumos bei nevirsyti oponento turimu pinigu.");
                    out("iveskite nauja suma:");
                    bet = inInt();
                }
                choise1 = inLine(game.getP2().getName() + ", jeigu norite sulyginti statymo suma, spauskite Y, jeigu norite pakelti statymo suma, spauskite P, jeigu norite pasiduoti, spauskite N");
                if (choise1.equalsIgnoreCase("Y")) {
                    out("Jusu statymo suma yra " + bet);
                    pot = bet * 2;
                } else if (choise1.equalsIgnoreCase("N")) {
                    game.getP1().setCash(game.getP1().getCash() + bet);
                    game.getP2().setCash(game.getP2().getCash() - bet);
                    break;
                } else {
                    int bet2 = inInt(game.getP2().getName() + ", iveskite nauja statymo suma: ");
                    while (bet2 <= bet) {
                        out("Jusu statymas negali buti lygus pries tai buvusiam ar uz ji mazesnis.");
                        out("iveskite nauja suma:");
                        bet2 = inInt();
                    }
                    while (bet2 <= 0 || bet2 > game.getP1().getCash() || bet2 > game.getP2().getCash()) {
                        out("Jusu statymas turi buti tarp 0 ir jusu turimos sumos bei nevirsyti oponento turimu pinigu.");
                        out("iveskite nauja suma:");
                        bet2 = inInt();
                    }
                    choise2 = inLine(game.getP1().getName() + ", jeigu norite sulyginti statymus, spauskite Y, jeigu norite pasiduoti, spauskite N");
                    if (choise2.equalsIgnoreCase("N")) {
                        game.getP1().setCash(game.getP1().getCash() - bet);
                        game.getP2().setCash(game.getP2().getCash() + bet);
                        System.exit(0);
                    } else if (choise2.equalsIgnoreCase("Y")) {
                        out("Jusu statymo suma yra " + bet2);
                        pot = bet2 * 2;
                    }
                }
            }
            break;
        }
    }

    public static void bettingP2IsActive(Game game) {
        boolean play = true;
        while (play) {
            if (game.getActivePlayer().equals(game.getP2())) {
                int bet = inInt(game.getP2().getName() + ", statykite pinigu suma: ");
                while (bet <= 0 || bet > game.getP1().getCash() || bet > game.getP2().getCash()) {
                    out("Jusu statymas turi buti tarp 0 ir jusu turimos sumos bei nevirsyti oponento turimu pinigu.");
                    out("iveskite nauja suma:");
                    bet = inInt();
                }
                choise1 = inLine(game.getP1().getName() + ", jeigu norite sulyginti statymo suma, spauskite Y, jeigu norite pakelti statymo suma, spauskite P, jeigu norite pasiduoti, spauskite N");
                if (choise1.equalsIgnoreCase("y")) {
                    out("Jusu statymo suma yra " + bet);
                    pot = bet * 2;
                } else if (choise1.equalsIgnoreCase("N")) {
                    game.getP2().setCash(game.getP2().getCash() + bet);
                    game.getP1().setCash(game.getP1().getCash() - bet);
                    break;
                } else {
                    int bet2 = inInt(game.getP1().getName() + ", iveskite nauja statymo suma: ");
                    while (bet2 <= bet) {
                        out("Jusu statymas negali buti lygus pries tai buvusiam ar uz ji mazesnis.");
                        out("iveskite nauja suma:");
                        bet2 = inInt();
                    }
                    while (bet2 <= 0 || bet2 > game.getP1().getCash() || bet2 > game.getP2().getCash()) {
                        out("Jusu statymas turi buti tarp 0 ir jusu turimos sumos bei nevirsyti oponento turimu pinigu.");
                        out("iveskite nauja suma:");
                        bet2 = inInt();
                    }

                    choise2 = inLine(game.getP2().getName() + ", jeigu norite sulyginti statymus, spauskite Y, jeigu norite pasiduoti, spauskite N");
                    if (choise2.equalsIgnoreCase("N")) {
                        game.getP2().setCash(game.getP2().getCash() - bet);
                        game.getP1().setCash(game.getP1().getCash() + bet);
                        System.exit(0);
                    } else {
                        out("Jusu statymo suma yra " + bet2);
                        pot = bet2 * 2;
                    }
                }
            }
            break;
        }
    }
}
