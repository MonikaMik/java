/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import static lt.vcs.VcsUtils.out;

/**
 *
 * @author Monika
 */
public class PersonService {
     public static boolean save(Person person) {
        boolean result;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "vcs_17_04";
        String userName = "root";
        String password = "";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            Statement s = conn.createStatement();
            s.executeUpdate("insert into person values ("+ person.getId() + ", '" + person.getName() + "', '"
                    + person.getSurName() + "', " + person.getGender().getId() + ", "
                    + person.getAge() + ", '" + person.getEmail() + "')");
            result = true;
            out(result);
        } catch (Exception e) {
            result = false;
            out(result);
        }
        
        return result;
        
     }
     
     public static Integer getNewId() {
        Integer result = null;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "vcs_17_04";
        String userName = "root";
        String password = "";
        Connection conn = null;    
        try {
            conn = DriverManager.getConnection(url + dbName, userName, password);
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("select id from person order by id desc limit 1;");
            if (rs.next()) {
                result = rs.getInt(1) + 1;
            }
        } catch (Exception e) {
            out("Klaida: " + e.getMessage());
        }
        return result;
    }
   
}
