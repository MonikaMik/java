/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Monika
 */
public class Person extends PersonService {
    
    private String name;
    private String surName;
    private Gender gender;
    private int age;
    private String email;
    private int id;
    
    
    public Person(int id, String name, String surName, Gender gender, int age, String email) {
        this.id = PersonService.getNewId();
        this.name = name;
        this.surName = surName;
        this.gender = gender;
        this.age = age;
        this.email = email;
    }
    
    @Override
    public String toString(){
        return getClass().getSimpleName() + "(name=" + getName() + " gender=" + gender.getEnLabel() + ")";
    }

    public String getName() {
        return name;
    }

    public String getSurName() {
        return surName;
    }

    public Gender getGender() {
        Gender.getById(id);
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public int getId() {
        return id;
    }



    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

}
