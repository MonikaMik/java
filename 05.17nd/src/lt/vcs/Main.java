/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.out;

/**
 *
 * @author Monika
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Person person = new Person(PersonService.getNewId(),"Rytis", "Mik", Gender.MALE, 24, "rytu@yahoo.com");
        PersonService.save(person);

    }
    
}
