/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Monika
 */
public interface Named extends Idable {
    
    String getName() throws BadDataInputException;
    
    public static Object newObj() {
        return new Object();
    }
    
}
