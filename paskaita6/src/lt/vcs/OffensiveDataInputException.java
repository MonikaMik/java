/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Monika
 */
public class OffensiveDataInputException extends Exception{
     
    public OffensiveDataInputException(){
        super();
    }
    public OffensiveDataInputException(String message){
        super(message);
    }
}
