/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.List;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Monika
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Named g1 = Gender.FEMALE;
        Named g2 = null;
        try {
            g2 = new Person("hefif", "jhfewf");
        } catch (Exception e) {
        }
        List<Named> nameList = new ArrayList();
        nameList.add(g1);
        nameList.add(g2);
        for (Named named : nameList) {
            try {
                out(named.getName());
                if (named instanceof Idable) { //paziureti ar gali buti konvertuojamas
                   Idable idable = (Idable) named; 
                   out(idable.getId());
                }
                
            } catch (Exception e) {
            }

        }
        out("belekas");
        AbstraktusDaiktas ad = new Belekas();
        ad.bendrasFunkcionalumas();
        out("belekas2");
        AbstraktusDaiktas ad2 = new Belekas2();
        ad2.bendrasFunkcionalumas();
    }
}
