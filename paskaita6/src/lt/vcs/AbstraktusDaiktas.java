/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Monika
 */
public abstract class AbstraktusDaiktas implements Idable {
    
    public abstract Object naujasObjektas();
    
    public int getId(){
        return 55;
    }
    
    public void bendrasFunkcionalumas(){
        out("" + naujasObjektas() + getId());
    }
}
