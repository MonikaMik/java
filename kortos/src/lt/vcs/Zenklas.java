/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

/**
 *
 * @author Monika
 */
public enum Zenklas {
    K("kryziai"),
    V("vynai"),
    B("bugnai"),
    S("sirdys");
    
    private String ltLabel;
    
    private Zenklas(String ltLabel){
        this.ltLabel = ltLabel;
    }

    public String getLtLabel() {
        return ltLabel;
    }


    }

