/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Monika
 */
public class Player extends Person {
    
    private int dice;
    private int[] diceHand;

    //konstruktorius,neturi return, nes auto grazina
  //  public Player(String name, Gender gender) {
       // this(name);
        //this.dice = random(1, 6);
        //this.gender = gender;
       // int [] diceHand = {random(1, 6), random(1, 6), random(1, 6), random(1, 6), random(1, 6)};
        //this.diceHand = diceHand;
    //}
    
    public Player(String name, String email) throws Exception {
        super(name, email);
        this.dice = random(1, 6);
    }
    public Player(String name, String email, Gender gender)throws Exception {
        this(name, email);
        this.gender = gender;
    }
    public Player(String name, String email, String surName, Gender gender, int age)throws Exception {
        this(name, email, gender);
        setSurName(surName);
        setAge(age);
    }
    @Override
    public String toString(){
        return super.toString().replaceFirst("\\)", " dice=" + dice + ")");
        //vienas \ escapintu tik is vieno
    }
    
    public int getDice() {
        return dice;
    }

    public int[] getDiceHand() {
        return diceHand;
    }
    


}

