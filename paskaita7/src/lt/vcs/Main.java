/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Files;
import static lt.vcs.VcsUtils.*;

/**
 *
 * @author Monika
 */
public class Main {

    private static final String PVZ_FAILAS = "C:\\temp\\pvz.txt";
    private static final String TEMP_DIR = "C:\\temp";
    private static final String UTF_8 = "Utf-8";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        File pvzFile = new File(PVZ_FAILAS);
        BufferedReader br = null;
        String failoTekstas = "";
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(new FileInputStream(pvzFile), UTF_8));
            while ((line = br.readLine()) != null) {
                failoTekstas += line + System.lineSeparator();
                out(line);
            }
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pvzFile), UTF_8));
            bw.append(failoTekstas);
            bw.append("ketvirta eilute");
            bw.flush();
        } catch (Exception e) {
            out(e.getMessage());
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            }
        }

    }

    private static File newFile(String dir, String fileName) { //pavadinimas rasomas su pletiniu
        File result = null;
        if (fileName == null || fileName == null) {
            out("blogi parametrai");
        } else {
            File dira = new File(dir);
            if (dira.isDirectory()) {
                if (!dira.exists()) {
                    try {
                        dira.mkdirs();
                    } catch (Exception e) {
                        out(e.getMessage());
                    }
                }
                result = new File(dira, fileName);
                try {
                    result.createNewFile();
                } catch (Exception e) {
                    out(e.getMessage());
                }
            } else {
                out("ivedete bloga direktorija");
            }

        }

        return result;

    }
}
