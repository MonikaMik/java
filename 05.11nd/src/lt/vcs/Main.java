/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.HashMap;
import java.util.Map;

import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Monika
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        countLetters();
        countCertainLetters();
    }
    
    public static void countLetters() {
        String text = inLine("Iveskite teksta:").toUpperCase();
        char[] txt = text.replaceAll(" ", "").toCharArray();
        Map<Character, Integer> charCounter = new HashMap();

        for (char i : txt) {
        charCounter.put(i,charCounter.get(i) == null ? 1 : charCounter.get(i) + 1);
        }
        //nezinau kaip atspausdint pagal pasikartojimu skaiciu
        for (Character key : charCounter.keySet()) {
            out(key + " - " + charCounter.get(key));
        }
    }
    
    public static void countCertainLetters() {
        String text = inLine("Iveskite teksta:").toUpperCase();
        char[] txt = text.replaceAll(" ", "").toCharArray();
        Map<Character, Integer> charCounter = new HashMap();

        for (char i : txt) {
        charCounter.put(i, charCounter.get(i) == null ? 1 : charCounter.get(i) + 1);
        }

        String letters = inLine("Iveskite raides,atskirtas per kableli, kurias norite suskaiciuoti tekste").toUpperCase();
        char[] let = letters.replaceAll(" ", "").toCharArray();

        for (Character key : charCounter.keySet()) {
            for (char i = 0; i < let.length; i++) {
                if (let[i] == key) {
                out(key + " - " + charCounter.get(key));
            }
            }

            
        }
    }
}
