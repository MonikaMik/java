/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.Scanner;

/**
 *
 * @author Monika
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       showAbove100();

    }
    
    
    private static void getSum(){
        out("Iveskite 5 skaicius:");
        int number = 0;
        int sum = 0;
        int [] numbers = new int[5];
        for (int i = 0; i < numbers.length; i++) {
            number = inInt();
            sum+= number; 
            numbers[i] = number;
        }
        for (int i = 0; i < numbers.length; i++) {
            out(numbers[i] + "+");
        }
        out("=" + sum);
    } 
    
    private static void getStrings(){
        out("Iveskite 5 zodzius:");
        String ivestasZodis;
        String [] zodziai = new String[5];
        for (int i = 0; i < zodziai.length; i++) {
            ivestasZodis = sc.next();
            zodziai[i] = ivestasZodis;
        }
        for (int i = 0; i < zodziai.length; i++) {
            out(zodziai[i] + ", ");
        }
    }
    
    private static void calculateKmi(){
        out("Iveskite savo mase(kg):");
        double mase = sc.nextDouble();
        out("Iveskite savo ugi(m):");
        double ugis = sc.nextDouble();
        out("Jusu KMI yra:" + Math.round((mase/Math.pow(ugis, 2))*100.0)/100.0);   
    }
    
    private static void showAbove100(){
        out("Pasirinkite kiek skaiciu noresite ivesti:");
        int x = inInt();
        int [] numbers = new int[x];
        int number = 0;
        out("Iveskite tiek skaiciu, kiek pasitinkote:");
        for (int i = 0; i < numbers.length; i++) {
            number = inInt();
            numbers[i] = number;
        }
        out(" ");
        out("Skaiciai virs 100: ");
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > 100) {
            out(numbers[i] + " ");
        }
        }
     
    }
        
    
    private static Scanner sc = new Scanner(System.in);
    private static void out(String txt) {
        System.out.println(txt);
    }

    private static Scanner newScan(){
       return new Scanner(System.in);
    }
    
    private static int inInt(){
      return newScan().nextInt();
    }
    
}
