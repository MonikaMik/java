/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Monika
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String line = inLine("iveskite betkokius zodzius atskirtus kableliu");
        line = line.replaceAll(" ", "");
        String[] lineMas = line.split(",");
        List<String> lineList = createList();
        Set<String> lineSet = createSet(lineMas);
        Iterator<String> iter = lineList.iterator();
        while (iter.hasNext()) {
            String iterStr = iter.next();
            iter.remove();
        }
        out("------ lineList ---------");
        outCollection(lineList);
        out("------ lineSet ---------");
        outCollection(lineSet);
        out("------ lineList SORTED ---------");
        Collections.sort(lineList);
        outCollection(lineList);
        out("------ lineList SORTED reverse ---------");
        Collections.reverse(lineList);
        outCollection(lineList);
        Map<String, Integer> mapas = new HashMap();
        mapas.put("obuolys", 5);
        Integer val = mapas.get("obuolys");
        out(val);
        Set<String> raktai = mapas.keySet();
        for (String raktas : raktai) {
            //do anything u want
        }
        Collection<Integer> reiksmes = mapas.values();
        for (Integer reiksme : reiksmes) {
            //do anything u want
        }
        
        Player p1 = null;
        while (p1 == null) {
            try {
                p1 = new Player(inWord("ivesk varda"), inWord("ivesk email'a"));
            } catch (Exception e) {
                out(e.getMessage());
            }
        }
        
        try{
            Person per1 = new Person("", "");
        }catch (BadDataInputException | OffensiveDataInputException e) {
            out(e.getMessage());
        }catch (Exception e){
            out(e.getMessage());
        }
            
    }
    
    
    
    
    private static Set<String> createSet(String...strings){
        Set<String> result = new HashSet();
        if (null != strings) {
            result.addAll(Arrays.asList(strings));
        }
        return result;
        }
    
    
    private static List<String> createList(String...strings){
        List<String> result = new ArrayList();
        if (null != strings) {
            result.addAll(Arrays.asList(strings));
        }
        return result;
        }
    
    private static void outCollection(Collection col){
        if (null != col) {
            for (Object item : col) {
            out(item);
        }

        }
    }
    
}
    

