/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Cukrus
 */
public class VcsUtils {
    
    public static void out(Object txt) {
        System.out.println(timeNow() + " " + txt);
    }
    
    private static Scanner newScan() {
        return new Scanner(System.in);
    }
    
    public static int inInt() {
        return newScan().nextInt();
    }
    
    public static int inInt(String txt) {
        out(txt);
        return inInt();
    }
    
    public static String inWord() {
        return newScan().next();
    }
    
    public static String inWord(String txt) {
        out(txt);
        return inWord();
    }
    
    public static String inLine() {
        return newScan().nextLine();
    }
    
    public static String inLine(String txt) {
        out(txt);
        return inLine();
    }
    
    public static int random(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to + 1);
    }
    
    private static String timeNow() {
        SimpleDateFormat sdf = new SimpleDateFormat("'['HH:mm:ss:SSSS']'");
        return sdf.format(new Date());
    }
    
}
