/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static lt.vcs.VcsUtils.*;
import static lt.vcs.Gender.*;
/**
 *
 * @author Monika
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //overridintas toSring metodas: (tik player objecto)
        Player p1 = (new Player(inWord("Iveskite varda"), Gender.getById(inInt("Iveskite lyti"))));
        /*String name = inWord("Iveskite varda");
        int lytis = inInt("Iveskite lyti");
        Gender gen = Gender.getById(lytis);
        out(new Player(name, gen));*/
        Person per1 = p1;
        Object obj = p1;
        Object [] mas = {p1, per1, obj, "Stringas"};
        out(p1);
        out(per1);
        out(obj);
        
        User<Person> u1 = new User(per1);
        User<Player> u2 = new User(p1);
        out(u1.getPerson());
        out(u2.getPerson());
        
        List lst = new ArrayList();
        List<String> strList = new ArrayList();
        strList.add("bla");
        strList.add("bla");
        for (String bla : strList){
            out(bla);
        }
        
        Set<String> strSet = new HashSet();
        strList.add("bla");
        strList.add("bla");
        for (String bla : strSet){
            out(bla);
        }
        


        
        


    }
    
    
}
