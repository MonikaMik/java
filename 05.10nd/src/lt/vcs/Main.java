/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lt.vcs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;import java.util.List;
import static lt.vcs.VcsUtils.*;
/**
 *
 * @author Monika
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        didejimoTvarka();
        mazejimoTvarka();
    }


    public static void isItPalindrome(){
        out("Iveskite teksta:");
        String input = inLine();
        String beTarpu = input.replaceAll(" ", "");
        String beT = beTarpu.toLowerCase();

        if (beT.equals(new StringBuilder(beT).reverse().toString())) {
            out("Tai yra palindromas");
        }
        else {
            out("Tai nera palindromas");
        }
    }
    public static void didejimoTvarka(){
        int[] numbers = {2, 5, 1, 9, -999, 9879, 55, 26};
        List<Integer> naujas = new ArrayList<Integer>();
        for (int i= 0; i < numbers.length; i++)
        {
        naujas.add(numbers[i]);
        }
        Collections.sort(naujas);
        out((Arrays.toString(naujas.toArray())));

    }
    
    public static void mazejimoTvarka(){
        int[] numbers = {2, 5, 1, 9, -999, 9879, 55, 26};
        List<Integer> naujas = new ArrayList<Integer>();
        for (int i= 0; i < numbers.length; i++)
        {
        naujas.add(numbers[i]);
        }
        Collections.sort(naujas);
        Collections.reverse(naujas);
        out((Arrays.toString(naujas.toArray())));
    }
}

        
        
    
    

